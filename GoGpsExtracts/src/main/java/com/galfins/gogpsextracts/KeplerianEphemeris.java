package com.galfins.gogpsextracts;

public abstract class KeplerianEphemeris extends GnssEphemeris {
    public final int week;
    public final double tocS;
    public final int health;
    public final double ttxS;
    public final int iode;
    public final KeplerianModel keplerModel;
    public final double af0S;
    public final double af1SecPerSec;
    public final double af2SecPerSec2;

    protected KeplerianEphemeris(KeplerianEphemeris.Builder<?> builder) {
        super(builder);
        this.week = builder.week;
        this.tocS = builder.tocS;
        this.ttxS = builder.ttxS;
        this.af0S = builder.af0S;
        this.iode = builder.iode;
        this.af1SecPerSec = builder.af1SecPerSec;
        this.af2SecPerSec2 = builder.af2SecPerSec2;
        this.keplerModel = builder.keplerModel;
        this.health = builder.health;
    }

    public abstract static class Builder<T extends KeplerianEphemeris.Builder<T>> extends GnssEphemeris.Builder<T> {
        private int week;
        private double tocS;
        private int health;
        private double ttxS;
        private double af0S;
        private int iode;
        private double af1SecPerSec;
        private double af2SecPerSec2;
        private KeplerianModel keplerModel;

        protected Builder() {
        }

        public abstract T getThis();

        public T setTocS(double tocS) {
            this.tocS = tocS;
            return this.getThis();
        }

        public T setHealth(int health) {
            this.health = health;
            return this.getThis();
        }

        public T setIode(int iode) {
            this.iode = iode;
            return this.getThis();
        }

        public T setAf0S(double af0S) {
            this.af0S = af0S;
            return this.getThis();
        }

        public T setAf1SecPerSec(double af1SecPerSec) {
            this.af1SecPerSec = af1SecPerSec;
            return this.getThis();
        }

        public T setAf2SecPerSec2(double af2SecPerSec2) {
            this.af2SecPerSec2 = af2SecPerSec2;
            return this.getThis();
        }

        public T setWeek(int week) {
            this.week = week;
            return this.getThis();
        }

        public T setTtxS(double ttxS) {
            this.ttxS = ttxS;
            return this.getThis();
        }

        public T setKeplerianModel(KeplerianModel keplerModel) {
            this.keplerModel = keplerModel;
            return this.getThis();
        }
    }
}