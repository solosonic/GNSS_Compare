/*
 * Copyright (c) 2010, Eugenio Realini, Mirko Reguzzoni, Cryms sagl - Switzerland. All Rights Reserved.
 *
 * This file is part of goGPS Project (goGPS).
 *
 * goGPS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * goGPS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with goGPS.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */
package com.galfins.gogpsextracts.Glonass;

import com.galfins.gogpsextracts.Time;

/**
 * <p>
 * GPS broadcast ephemerides
 * </p>
 *
 * @author Eugenio Realini, Cryms.com, Daisuke Yoshida 
 */

public class EphGlonass {
	private final static int STREAM_V = 1;

	private Time refTime; /* Reference time of the dataset */
	private char satType; /* Satellite Type */
	private int satID; /* Satellite ID number */
	private int week; /* GPS week number */
	private double toe; /* ephemeris reference time */
	private double toc; /* clock data reference time */

	/* for GLONASS data */
	private float tow;

	private float tauN;
	private float gammaN;
	private double tk;

	private double X;
	private double Xv;
	private double Xa;
	private double Bn;

	private double Y;
	private double Yv;
	private double Ya;
	private int freq_num;
	private double tb;

	private double Z;
	private double Zv;
	private double Za;
	private double En;

  public static final EphGlonass UnhealthyEph = new EphGlonass();


	public EphGlonass(){

	}

	public EphGlonass(GlonassEphemeris ephemerids) {
		this.satID = ephemerids.svid;
		this.satType = 'R';
        this.week = ephemerids.week;

		this.toc = ephemerids.tocS;
		this.toe = ephemerids.keplerModel.toeS;

		this.tauN = ephemerids.tauN;
		this.gammaN = ephemerids.gammaN;
		this.tk = ephemerids.tk;

		this.X = ephemerids.X;
		this.Xv = ephemerids.Xv;
		this.Xa = ephemerids.Xa;
		this.Bn = ephemerids.Bn;

		this.Y = ephemerids.Y;
		this.Yv = ephemerids.Yv;
		this.Ya = ephemerids.Ya;
		this.freq_num = ephemerids.freq_num;
		this.tb = ephemerids.tb;

		this.Z = ephemerids.Z;
		this.Zv = ephemerids.Zv;
		this.Za = ephemerids.Za;
		this.En = ephemerids.En;
        this.refTime = new Time(week, toc, satType);
	}

    /**
	 * @return the refTime
	 */
	public Time getRefTime() {
		return refTime;
	}
	/**
	 * @param refTime the refTime to set
	 */
	public void setRefTime(Time refTime) {
		this.refTime = refTime;
	}
	/**
	 * @return the satType
	 */
	public char getSatType() {
		return satType;
	}
	/**
	 * @param satType the satType to set
	 */
	public void setSatType(char satType) {
		this.satType = satType;
	}
	/**
	 * @return the satID
	 */
	public int getSatID() {
		return satID;
	}
	/**
	 * @param satID the satID to set
	 */
	public void setSatID(int satID) {
		this.satID = satID;
	}
	/**
	 * @return the week
	 */
	public int getWeek() {
		return week;
	}
	/**
	 * @param week the week to set
	 */
	public void setWeek(int week) {
		this.week = week;
	}

	/**
	 * @return the toc
	 */
	public double getToc() {
		return toc;
	}
	/**
	 * @param toc the toc to set
	 */
	public void setToc(double toc) {
		this.toc = toc;
	}
	/**
	 * @return the toe
	 */
	public double getToe() {
		return toe;
	}
	/**
	 * @param toe the toe to set
	 */
	public void setToe(double toe) {
		this.toe = toe;
	}

	/* for GLONASS data */

	public float getTauN() {
		return tauN;
	}
	public void setTauN(float tauN) {
		this.tauN = tauN;
	}

	public float getGammaN() {
		return gammaN;
	}
	public void setGammaN(float gammaN) {
		this.gammaN = gammaN;
	}

	public double gettk() {
		return tk;
	}
	public void settk(double tk) {
		this.tk = tk;
	}

	public double getX() {
		return X;
	}
	public void setX(double X) {
		this.X = X;
	}

	public double getXv() {
		return Xv;
	}
	public void setXv(double Xv) {
		this.Xv = Xv;
	}

	public double getXa() {
		return Xa;
	}
	public void setXa(double Xa) {
		this.Xa = Xa;
	}

	public double getBn() {
		return Bn;
	}
	public void setBn(double Bn) {
		this.Bn = Bn;
	}

	public double getY() {
		return Y;
	}
	public void setY(double Y) {
		this.Y = Y;
	}

	public double getYv() {
		return Yv;
	}
	public void setYv(double Yv) {
		this.Yv = Yv;
	}

	public double getYa() {
		return Ya;
	}
	public void setYa(double Ya) {
		this.Ya = Ya;
	}

	public int getfreq_num() {
		return freq_num;
	}
	public void setfreq_num(int freq_num) {
		this.freq_num = freq_num;
	}

	public double gettb() {
		return tb;
	}
	public void settb(double tb) {
		this.tb = tb;
	}

	public double getZ() {
		return Z;
	}
	public void setZ(double Z) {
		this.Z = Z;
	}

	public double getZv() {
		return Zv;
	}
	public void setZv(double Zv) {
		this.Zv = Zv;
	}

	public double getZa() {
		return Za;
	}
	public void setZa(double Za) {
		this.Za = Za;
	}

	public double getEn() {
		return En;
	}
	public void setEn(double En) {
		this.En = En;
	}

	//	public long getEn() {
	//		return En;
	//	}
	//	public void setEn(long En) {
	//		this.En = En;
	//	}

}
